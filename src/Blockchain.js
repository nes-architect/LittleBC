let sha256 = require("sha256");
let fs = require("fs");
let WebSocket = require("ws");

let Block = require("./obj/Block");
let ServerP2P = require("./ServerP2P");

/**
 * List of initial PEERS to connect to them
 * @type {Array}
 */
let initialPeers = require("../config").initialPeer || [
    "ws://46.105.46.52:6001",
];

let Blockchain = {

    /**
     * Create the hash for a specified Block
     * @param block
     * @returns {string}
     */
    calculateBlockHash: function(block){
        // Return the hash of the block by concatenate the index, prevHash, timestamp and the data
        return sha256(block.index+block.prevHash+block.timestamp+block.data).toString();
    },

    /**
     * Calculate a hash for individual key data of block (for a new block)
     * @param index
     * @param prevHash
     * @param timestamp
     * @param data
     */
    calculateHash: function(index,prevHash,timestamp,data){
        return sha256(index+prevHash+timestamp+data).toString();
    },

    /**
     * Check if the given block is valide
     * @param block
     * @param previousBlock
     */
    isValidBlock: function(block, previousBlock){

        if(block.index !== previousBlock.index+1){
            console.log("[MAIN] [ERR] The 'new' block with index",block.index,"did't follow the previous index",previousBlock.index);
            return false;
        }
        else if(previousBlock.hash !== block.prevHash){
            console.log("[MAIN] [ERR] The block #",block.index,"has a different previous hash (",block.prevHash,") not match with the real hash",previousBlock.hash);
            return false;
        }
        else if(this.calculateBlockHash(block) !== block.hash){
            console.log("[MAIN] [ERR] The block The block #",block.index," has a different hash",block.hash," != ",this.calculateBlockHash(block));
            return false;
        }

        return true;

    },

    /**
     * Check if the full chain is valid
     * @returns {boolean}
     */
    isChainValid: function(){
        let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));

        // Check the genesis block
        if( JSON.stringify(Block.fromFILE("0.blk")) !== JSON.stringify(this.getGenesisBlock()) ){
            console.log("[MAIN] [ERR] GENESIS is died, please remove file datas/0.blk to regenerate it when restarting");
            return false;
        }

        /**
         * Check all Blocks one by one
         */
        for(let i=1; i < files.length; i++){
            let block = Block.fromFILE(files[i]);
            if(!this.isValidBlock(block, Block.fromFILE(files[i-1]))){
                console.log("[MAIN] [ERR] Block #",block.index,"is not valid");
                return false;
            }
        }

        // if all blocks are valid we return true
        return true;
    },

    /**
     * Return the GENESIS block
     * @returns {Block}
     */
    getGenesisBlock: function(){

        let genesis = new Block(
            0,                                                                  // index 0 because it's the first Block :)
            "0",                                                                // It didn't have any hash of course
            "1510741697",                                                       // The timestamp
            "This is the first block",                                          // The content of the block, a simple sentence
            "b02e03943a08ad287c42af3c9a898a4ce4c31ef007b5e77ecb5eb6e7ea1878a7"  // The hash of this block
        );

        /**
         * Try to load the genesis block
         */
        try {
            let genesisFile = Block.fromJSON( JSON.parse(fs.readFileSync("./datas/0.blk")) );
            return genesis;
        }
        /**
         * If not try to crete it
         */
        catch(err){
            console.log("[MAIN] [WAR] Genesis didn't exists... creation in progress");
            let rawdata = JSON.stringify(genesis);
            fs.writeFileSync("./datas/0.blk", rawdata);
            console.log("[MAIN] [INF] Genesis block created !");
            return genesis;
        }
    },

    /**
     * Create a new block on the disk
     * @param block
     */
    addNewBlock: function(block){

        if(this.isValidBlock(block, this.getLatestBlock())){
            let rawdata = JSON.stringify(block);
            fs.writeFileSync("./datas/"+block.index+".blk", rawdata);
        }
        else{
            console.log("[MAIN] [ERR] The block with index #",block.index,"and hash",block.hash,"is not valid ! skipped");
        }

    },

    /**
     * Replace a valid block by a new one (used when a conflict block became a legit block)
     * @param index
     * @param newBlock
     * @return boolean
     */
    replaceBlock: function(oldBlock, newBlock){

        let fs = require("fs");
        // remove old block
        fs.unlinkSync("./datas/"+oldBlock.index+".blk");
        // Add the new one
        Blockchain.addNewBlock(newBlock);

    },

    /**
     * Return block from specified index if index is lower than local chain
     * @param index
     * @returns {boolean}
     */
    getBlock: function(index){

        if(index > Blockchain.getLatestBlock().index){
            return false
        }
        else{
            let fs = require("fs");
            let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));
            return Block.fromFILE(files[index]);
        }


    },

    /**
     * Return the lastest block on the local chain (not the latest on the network)
     * @return Block | boolean
     */
    getLatestBlock: function(){

        if(!Blockchain.isChainValid()){
            console.log("[MAIN] [KO] Chain is not valid x_x and need to be synced");
            return false;
        }
        else{
            let fs = require("fs");
            let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));
            return Block.fromFILE(files[files.length-1]);
        }

    },

    getAllBlocks: function(){
        if(!Blockchain.isChainValid()){
            console.log("[MAIN] [KO] Chain is not valid x_x and need to be synced");
            return false;
        }
        else{
            let fs = require("fs");
            let blocks = [];
            let files = fs.readdirSync("./datas/").sort((a,b)=>(a.split(".blk")[0]-b.split(".blk")[0]));
            for(let i = 0; i < files.length; i++){
                blocks.push(Block.fromFILE(files[i]));
            }

            return blocks;
        }
    },

    /**
     * Generate a new block from the specified dataBlock
     * @param dataBlock
     */
    generateNextBlock: function(dataBlock){

        if(Blockchain.isChainValid()){
            let previousBlock = this.getLatestBlock();

            let nextIndex = previousBlock.index+1;
            let nextPrevHash = previousBlock.hash;
            let nextTimestamp = new Date().getTime();
            let nextHash = this.calculateHash(nextIndex,nextPrevHash,nextTimestamp,dataBlock);

            return new Block(nextIndex,nextPrevHash,nextTimestamp,dataBlock,nextHash);
        }
        else{
            console.log("[MAIN] [KO] Chain is not valid x_x can't generate new block til the chain is not valid");
            return false;
        }

    },

    /**
     * Init the connection to all know peers
     */
    connectToPeers: function(peerList){

        if(peerList === undefined) peerList = initialPeers;

        peerList.forEach(function(peer){
           let socket = new WebSocket(peer);

           socket.on('open', function(){ServerP2P.initConnect(socket)});
           socket.on('error', function(){console.log("[P2P] Failed to connect to peer",socket.url)});
        });
    },

    /**
     * Return the initial Peer list
     * @return {Array}
     */
    getInitialPeers: function(){
        return initialPeers;
    }

}

module.exports = Blockchain;
