let Websocket = require("ws");
let P2P_PORT = require("../config").p2p_port;

let Message = require("./obj/Message");
let Block = require("./obj/Block");

/**
 * Array of all connected peers
 * @type {Array}
 */
let peers_sockets = [];

/**
 * Contains the conflicted blocks
 * @type {Array}
 */
let conflictedBlock = null;

/**
 * Constructor of the Server
 * @constructor
 */
let ServerP2P = function(){
    console.log("[MAIN] [INF] Starting the P2P server...");

    let p2p = new Websocket.Server({port: P2P_PORT});
    let that = this;
    p2p.on("connection", function(newWS){
        ServerP2P.initConnect(newWS); //--> For incomming connection
    });

    console.log("[MAIN] [OK] P2P Server listening on", P2P_PORT);

};

/**
 * On new connection from other Peers
 * @param newWS
 */
ServerP2P.initConnect = function(peer){

    if(peer.url === undefined){
        console.log("[P2P] [INF] New Peer connection from (",peer._socket.remoteAddress.replace("::ffff:",""),")");
    }
    else{
        console.log("[P2P] [INF] Connected to peer (",peer.url,")");
    }
    peers_sockets.push(peer);
    console.log("[P2P] [INF] Total peers :",peers_sockets.length);

    /**
     * Share the peer list to the network
     */
    ServerP2P.broadcast(ServerP2P.Message_responseSharePEERS());

    /**
     * to process all messages received from peers
     */
    ServerP2P.initMessageHandler(peer);

    /**
     * to process all errors or closing connexion from peers
     */
    ServerP2P.initErrorHandler(peer);

    /**
     * Ask to the latest block on each new Peers to see if they didn't have more block than this server
     */
    ServerP2P.writeMsg(peer, ServerP2P.Message_queryLatestBlock());
    console.log("[P2P] [INF] Ask to the latest block to the new peers...");
};

/**
 * Link all function related to the message between all peers when socket received an event type 'message'
 * @param peer
 */
ServerP2P.initMessageHandler = function(peer){
    peer.on('message', function(data){

       let message = JSON.parse(data);

        switch(message.type){

            /**
             * To ask the LATEST block on the remote peer
             */
            case Message.MESSAGES_TYPE.QUERY_LATEST:
               ServerP2P.writeMsg(peer, ServerP2P.Message_responseLatestBlock() );
               break;

            /**
             * To ask ALL blocks on the remote peer (not good if she's to big !)
             * in the Naive project the full blockchain was requested.
             *
             * It will be removed later
             */
            case Message.MESSAGES_TYPE.QUERY_ALL:
               ServerP2P.writeMsg(peer, ServerP2P.Message_responseAllBlock() );
               break;

            /**
             * To ask a missing BLOCK on the remote peer
             */
            case Message.MESSAGES_TYPE.QUERY_BLOCK:
                ServerP2P.writeMsg(peer, ServerP2P.Message_responseNextMissingBlock(message.data));
                break;

            /**
             * Process the response received by a peer
             */
            case Message.MESSAGES_TYPE.RESPONSE_BLOCKCHAIN:
               //console.log("[P2P] [MSG] Received response from :",peer._socket.remoteAddress);
               ServerP2P.HandlerBlockchainMessage(message);
               break;

            /**
             * We received list of valid peers
             */
            case Message.MESSAGES_TYPE.SHARE_PEER_LIST:
                console.log("[P2P] [MSG] Received new peers list from :",peer._socket.remoteAddress.replace("::ffff:",""));
                ServerP2P.HandlerSharePEERSMessage(message);
                break;

           default:
               console.log("[P2P] [WAR] Unknown message type x_x");
               break;
       }
    });
};

/**
 * Connect close/error events on a function to close the connection with the peer in error state
 * @param peer
 */
ServerP2P.initErrorHandler = function(peer){

   let closeCxn = function(peer){

       console.log("[P2P] [ERR] Connection failed or lost with peer :",peer.url||peer._socket.remoteAddress.replace("::ffff:",""));
       peers_sockets.splice(peers_sockets.indexOf(peer), 1);
       console.log("[P2P] [INF] Total peers :",peers_sockets.length);

   };

   /**
   * Map the previous function with event close/error
   * */
   peer.on('close', function(){closeCxn(peer)});
   peer.on('error', function(){closeCxn(peer)});
}

/**
 * Processing the message received by a peer
 * @param message
 * @constructor
 */
ServerP2P.HandlerBlockchainMessage = function(message){

    let BlockChain = require("./Blockchain");

    /**
     * Parse Blocks and order by their index
     * @type {Array.<T>}
     */
    let Peer_latestBlock = Block.fromJSON( JSON.parse(message.data.block) );
    let Local_latestBlock = BlockChain.getLatestBlock();

    if(Peer_latestBlock.index > Local_latestBlock.index){

        /**
         * Do we have a pending conflict block ?
         * -> if yes and it's match with the conflicted, the conflicted became the legit block
         * -> else and it match with the in place block, the conflict block is forget
         */
        if(conflictedBlock !== null && conflictedBlock.hash === Peer_latestBlock.prevHash){
            console.log(("[P2P] [INF] The pending block in conflict state is the legit chain ! continue with it"));
            BlockChain.replaceBlock(BlockChain.getLatestBlock(), conflictedBlock);
            BlockChain.addNewBlock(Peer_latestBlock);
            ServerP2P.broadcast(ServerP2P.Message_responseLatestBlock());

            // Check if the Local chain is the highest ?
            ServerP2P.broadcast(ServerP2P.Message_queryLatestBlock());

            // Delete the conflicted block
            conflictedBlock = null;
        }
        /**
         * If the block is the next than our latest local block and we didn't have any conflict
         */
        else if(conflictedBlock === null && Local_latestBlock.hash === Peer_latestBlock.prevHash){

            console.log(("[P2P] [INF] New block #"+Peer_latestBlock.index+" added from peer"));
            BlockChain.addNewBlock(Peer_latestBlock);
            ServerP2P.broadcast(ServerP2P.Message_responseLatestBlock());

            // Check if the Local chain is the highest ?
            ServerP2P.broadcast(ServerP2P.Message_queryLatestBlock());

            // Delete the conflicted block
            conflictedBlock = null;

        }
        /**
         * Else the block is more far than 1 block in the futur...
         */
        else{
            //console.log("[P2P] [INF] We have to query missing blocks from our peer");
            ServerP2P.broadcast(ServerP2P.Message_queryNextMissingBlock(Local_latestBlock.index+1));
        }
    }
    else if( Local_latestBlock.hash !== Peer_latestBlock.hash && Local_latestBlock.index === Peer_latestBlock.index && Local_latestBlock.prevHash === Local_latestBlock.prevHash){
        console.log("[P2P] [WAR] Block conflict detected 0_0.");
        conflictedBlock = Peer_latestBlock;
    }
    else{
        console.log("[P2P] [INF] Received blockchain is lower than our, do nothing.");
    }

    return {};
};

/**
 * Manage active peer list
 * @constructor
 */
ServerP2P.HandlerSharePEERSMessage = function(message){
    let messagePeersURL = JSON.parse(message.data);

    /**
     * Remove the server from the remote peer list
     * @type {Array.<*>}
     */
    let remotePeersURL = peers_sockets.filter((peer)=>(peer._isServer));
    remotePeersURL = remotePeersURL.map((peer)=>(peer._socket._peername.address.replace("::ffff:","")));

    /**
     * Determine the new peer received
     * @type {Array.<*>}
     */
    let newPeersURL = remotePeersURL.filter((peer)=>(peer.indexOf(messagePeersURL)===-1));
    console.log(newPeersURL);

};

/**
 * Write a message to a peer with a response function
 */
ServerP2P.writeMsg = function(peer, message){
    peer.send(JSON.stringify(message));
};

/**
 * Prepare a message to ask the last Block
 * @returns {{type: number}}
 * @constructor
 */
ServerP2P.Message_queryLatestBlock = function(){
    return {type: Message.MESSAGES_TYPE.QUERY_LATEST};
};

/**
 * Prepare a message to ask all blocks (not good)
 * Will be removed
 * @returns {{type: number}}
 * @constructor
 */
ServerP2P.Message_queryAllBlocks = function(){
    return {type: Message.MESSAGES_TYPE.QUERY_ALL};
};

/**
 * Return a 'ready' message to ask a specific block between lowIndex and highIndex (will be implemented whith OrphanBlocks support)
 * @param lowIndex, highIndex
 * @returns {{type: number, data: {lowIndex: number, highIndex: number}}}
 * @constructor
 */
ServerP2P.Message_queryBlock = function(lowIndex, highIndex){
    return {type: Message.MESSAGES_TYPE.QUERY_RANDOM_BLOCK, data: {lowIndex:lowIndex,highIndex:highIndex}};
};

/**
 * Return a 'ready' message to ask the next missing block (until Orphan Block are not implemented)
 * @param blockIndex
 * @returns {{type: number, data: *}}
 * @constructor
 */
ServerP2P.Message_queryNextMissingBlock = function(blockIndex){
    return {type: Message.MESSAGES_TYPE.QUERY_BLOCK, data: blockIndex};
};

/**
 * Return a 'ready' message with the latest block and the size of the current local chain
 * @returns {{type: number, data}}
 * @constructor
 */
ServerP2P.Message_responseLatestBlock = function(){
    let BlockChain = require("./Blockchain");
    return {
        type: Message.MESSAGES_TYPE.RESPONSE_BLOCKCHAIN,
        data: {
            block: JSON.stringify(BlockChain.getLatestBlock()),
            highest: BlockChain.getLatestBlock().index
        }
    }
};

/**
 * Return a 'ready' message with all blocks
 * @returns {{type: number, data}}
 * @constructor
 */
ServerP2P.Message_responseAllBlock = function(){
    let BlockChain = require("./Blockchain");
    return {
        type: Message.MESSAGES_TYPE.RESPONSE_BLOCKCHAIN,
        data: JSON.stringify(BlockChain.getAllBlocks())
    }
};

/**
 * Return a 'ready' message with the asked missing block for the remote peer with the size of the local chain
 * @returns {*}
 * @constructor
 */
ServerP2P.Message_responseNextMissingBlock = function(index){
    let BlockChain = require("./Blockchain");
    let askedBlock = BlockChain.getBlock(index);

    //--> Nothing to send to the peer, not concerned because the asked index did not exists in the local chain
    if(!askedBlock){
        return false;
    }
    else{
        return {
            type: Message.MESSAGES_TYPE.RESPONSE_BLOCKCHAIN,
            data: {
                block: JSON.stringify(askedBlock),        // send the missing block asked
                highest: BlockChain.getLatestBlock().index  // send the highest index in the local chain of this peer
            }
        }
    }

};

/**
 * Return a 'ready' message with list of connected peer
 * @return {{type: number, data: {peers_sockets: Array}}}
 * @constructor
 */
ServerP2P.Message_responseSharePEERS = function(){

    let remotePeersURL = peers_sockets.filter((peer)=>(peer._isServer));
    remotePeersURL = remotePeersURL.map((peer)=>(peer._socket._peername.address.replace("::ffff:","")));

    return {
        type: Message.MESSAGES_TYPE.SHARE_PEER_LIST,
        data: JSON.stringify(remotePeersURL)
    }
}

/**
 * Write a message to all connected Peers
 * @param message
 */
ServerP2P.broadcast = function(message){
    peers_sockets.forEach(function(peer){
        ServerP2P.writeMsg(peer, message);
    })
};

module.exports = ServerP2P;