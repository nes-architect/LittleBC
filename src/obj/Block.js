let Block = function(index, prevHash, timestamp, data, hash){
    this.index = index;
    this.prevHash = prevHash.toString();
    this.timestamp = timestamp;
    this.data = data;
    this.hash = hash;
};

Block.fromJSON = function(json){
    return new Block(
        json.index,
        json.prevHash,
        json.timestamp,
        json.data,
        json.hash
    )
};

Block.fromFILE = function(fileBLK){
    let fs = require("fs");
    return Block.fromJSON( JSON.parse(fs.readFileSync("./datas/"+fileBLK)) );
}

module.exports = Block;