module.exports = {
    /**
     * Define the port of HTTP server and the P2P server
     */
    http_port: 3001,
    p2p_port: 6001,

    /**
     * Define the verbosity of the console output
     */
    console_level: 0,

    initialPeer: [
        "ws://46.105.46.52:6001"
    ]
}